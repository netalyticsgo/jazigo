package dev

import (
	"time"

	"gitlab.com/netalyticsgo/jazigo/conf"
)

func registerModelAruba(logger hasPrintf, t *DeviceTable) {
	a := conf.NewDevAttr()

	a.NeedLoginChat = true
	a.NeedEnabledMode = false
	a.NeedPagingOff = false
	a.EnableCommand = ""
	a.UsernamePromptPattern = `Username:\s*$`
	a.PasswordPromptPattern = `Password:\s*$`
	a.EnablePasswordPromptPattern = `Password:\s*$`
	a.DisabledPromptPattern = `\S+>\s*$`
	a.EnabledPromptPattern = `\S+#\s*$`
	a.CommandList = []string{"show ver", "show run"}
	a.DisablePagerCommand = ""
	a.ReadTimeout = 10 * time.Second
	a.MatchTimeout = 20 * time.Second
	a.SendTimeout = 5 * time.Second
	a.CommandReadTimeout = 20 * time.Second  // larger timeout for slow 'sh run'
	a.CommandMatchTimeout = 30 * time.Second // larger timeout for slow 'sh run'
	a.QuoteSentCommandsFormat = `!![%s]`

	m := &Model{name: "aruba"}
	m.defaultAttr = a
	if err := t.SetModel(m, logger); err != nil {
		logger.Printf("registerModelAruba: %v", err)
	}
}
