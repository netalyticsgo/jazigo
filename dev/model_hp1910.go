package dev

import (
	"time"

	"gitlab.com/netalyticsgo/jazigo/conf"
)

func registerModelHp1910(logger hasPrintf, t *DeviceTable) {
	a := conf.NewDevAttr()

	a.NeedLoginChat = true
	a.NeedEnabledMode = true
	a.NeedPagingOff = true
	a.EnableCommand = "_cmdline-mode on"
	a.EnableCommandInteractive = "Y"
	a.UsernamePromptPattern = `Username:\s*$`
	a.PasswordPromptPattern = `Password:\s*$`
	a.EnablePasswordPromptPattern = `password:\s*$`
	a.DisabledPromptPattern = `\S+>\s*$`
	a.EnabledPromptPattern = `\S+>\s*$`
	a.CommandList = []string{"display current-configuration", " "}
	a.DisablePagerCommand = "screen-length disable"
	a.ReadTimeout = 10 * time.Second
	a.MatchTimeout = 20 * time.Second
	a.SendTimeout = 5 * time.Second
	a.CommandReadTimeout = 20 * time.Second  // larger timeout for slow 'sh run'
	a.CommandMatchTimeout = 30 * time.Second // larger timeout for slow 'sh run'
	a.QuoteSentCommandsFormat = `!![%s]`

	m := &Model{name: "hp-1910"}
	m.defaultAttr = a
	if err := t.SetModel(m, logger); err != nil {
		logger.Printf("registerModelHP1910: %v", err)
	}
}
