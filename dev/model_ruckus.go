package dev

import (
	"gitlab.com/netalyticsgo/jazigo/conf"
	"time"
)

func registerModelRuckusController(logger hasPrintf, t *DeviceTable) {
	a := conf.NewDevAttr()

	a.NeedLoginChat = true
	a.NeedEnabledMode = false
	a.NeedPagingOff = false
	a.EnableCommand = "enable"
	a.UsernamePromptPattern = `Please login:\s*$`
	a.PasswordPromptPattern = `Password:\s*$`
        a.DisabledPromptPattern = `\S+>\s*$`
        a.EnabledPromptPattern = `\S+#\s*$`
	a.EnablePasswordPromptPattern = ""
	a.CommandList = []string{"", "show config","show sysinfo"} // "" = dont send, wait for command prompt
	a.DisablePagerCommand = ""
	a.ReadTimeout = 5 * time.Second
	a.MatchTimeout = 10 * time.Second
	a.SendTimeout = 5 * time.Second
	a.CommandReadTimeout = 10 * time.Second
	a.CommandMatchTimeout = 10 * time.Second
	a.QuoteSentCommandsFormat = `##[%s]`

	m := &Model{name: "ruckus_controller"}
	m.defaultAttr = a
	if err := t.SetModel(m, logger); err != nil {
		logger.Printf("registerModelRuckusController: %v", err)
	}
}
