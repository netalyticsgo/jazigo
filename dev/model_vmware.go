package dev

import (
	"time"

	"gitlab.com/netalyticsgo/jazigo/conf"
)

func registerModelVmware(logger hasPrintf, t *DeviceTable) {
	a := conf.NewDevAttr()

	a.NeedLoginChat = true
	a.NeedEnabledMode = false
	a.NeedPagingOff = false
	a.EnableCommand = ""
	a.UsernamePromptPattern = `Username:\s*$`
	a.PasswordPromptPattern = `Password:\s*$`
        promptPattern := `\S+~]*$` // "hostname # "
	a.EnablePasswordPromptPattern = ""
	a.DisabledPromptPattern = promptPattern
	a.EnabledPromptPattern = promptPattern
	a.CommandList = []string{"", "esxcli system version get"} // "" = dont send, wait for command prompt
	a.DisablePagerCommand = ""
	a.ReadTimeout = 5 * time.Second
	a.MatchTimeout = 10 * time.Second
	a.SendTimeout = 5 * time.Second
	a.CommandReadTimeout = 10 * time.Second
	a.CommandMatchTimeout = 10 * time.Second
	a.QuoteSentCommandsFormat = `##[%s]`

	m := &Model{name: "vmware"}
	m.defaultAttr = a
	if err := t.SetModel(m, logger); err != nil {
		logger.Printf("registerModelVmware: %v", err)
	}
}
